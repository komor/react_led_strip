import React, { forwardRef, useImperativeHandle, useEffect} from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";

import CardContent from "@material-ui/core/CardContent";
import CardActions from "@material-ui/core/CardActions";
import Collapse from "@material-ui/core/Collapse";

import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import { red } from "@material-ui/core/colors";

import MoreVertIcon from "@material-ui/icons/MoreVert";

import Slider from "@material-ui/core/Slider";
import Switch from "@material-ui/core/Switch";

import Box from "@material-ui/core/Box";
import { withStyles } from "@material-ui/core/styles";
import { createMuiTheme } from "@material-ui/core";
import Fab from "@material-ui/core/Fab";
import WbIncandescentIcon from "@material-ui/icons/WbIncandescent";
import { green } from "@material-ui/core/colors";
import ColorPicker from "./ColorPicker";
import RegionMenu from "./RegionMenu";
import EditMenu from "./EditMenu";

import { connect } from "react-redux";
import * as regionAction from "../actions/regionAction";
import {RegionData, Range, Effect, Color} from '../data_structures';


const RegionManager = require('../services/RegionManager');


export const theme = createMuiTheme({
  overrides: {
    MuiFormControlLabel: {
      label: {
        fontSize: "10px !important"
      }
    }
  }
});

const useStyles = makeStyles(theme => ({
  card: {
    display: "flex",
    flexDirection: "column",
    //  width: 300,
    flexGrow: 1,
    borderRadius: 20
  },
  header: {
    display: "flex",
    flexDirection: "row",
    flexGrow: 1,
    justifyContent: "center !important",
    alignItems: "center !important",
    paddingBottom: "0 !important"
  },
  content: {
    display: "flex",
    flexDirection: "column",
    flexGrow: 1,
    paddingBottom: "0 !important",
    paddingTop: "0 !important"
  },
  slider: {
    display: "flex",
    padding: theme.spacing(2, 2)
  },
  fx: {
    display: "flex",
    flexDirection: "column",
    flexGrow: 1,
    backgroundColor: "#99cc00",
    borderRadius: 20,
    alignItems: "center",
    justifyContent: "center",
    minHeight: 50,
    paddingBottom: 0

    /*
    justifyContent: 'center',
    alignItems: 'center', */
  },
  brightness: {
    display: "flex",
    flexDirection: "column",
    flexGrow: 1
  },

  actions: {
    display: "flex",
    flexDirection: "row",
    flexGrow: 1,
    padding: theme.spacing(2, 2)
    //  borderSpacing: 20
  },
  more: {
    display: "flex",
    flexDirection: "row",

    justifyContent: "center",
    alignItems: "flex-start"
    //  padding : theme.spacing(0,0)
  },
  button: {
    display: "flex",
    flexDirection: "row",
    flexGrow: 2,
    justifyContent: "center",
    alignItems: "center",
    padding: theme.spacing(0, 0)
  },
  label: {
    fontSize: "2px !important"
  },
  gap: {
    display: "flex",
    flexDirection: "row",
    flexGrow: 10,
    justifyContent: "center",
    alignItems: "flex-end"
  },
  expand: {
    transform: "rotate(0deg)",
    marginLeft: "auto",
    flexGrow: 1,

    transition: theme.transitions.create("transform", {
      duration: theme.transitions.duration.shortest
    })
  },
  expandOpen: {
    transform: "rotate(180deg)"
  },
  avatar: {
    backgroundColor: red[500]
  },
  iconHover: {
    color: theme.palette.common.white,
    "&:hover": {
      backgroundColor: green[80]
    }
  }
}));

const PrettoSlider = withStyles({
  root: {
    // color: theme.,
    height: 10
  },
  thumb: {
    height: 24,
    width: 24,
    backgroundColor: "#fff",
    border: "2px solid currentColor",
    marginTop: -4,
    marginLeft: -12,
    "&:focus,&:hover,&$active": {
      boxShadow: "inherit"
    }
  },
  active: {},
  valueLabel: {
    left: "calc(-50% + 4px)"
  },
  track: {
    height: 14,
    borderRadius: 8
  },
  rail: {
    height: 14,
    borderRadius: 8
  }
})(Slider);


const getIconColor = state =>{
  if (state === "off")
    return "#709900";
  else
    return "#ffffff";
}


const Region = forwardRef((props, ref) => {
  const classes = useStyles();
  const initialValue = [];

  const [expanded] = React.useState(false);

  const [openFx, setOpenFx] = React.useState(false);
  const [openEdit, setOpenEdit] = React.useState(false);
 // const [color, setColor] = React.useState(props.data.fx.params.color);
  const [state, setState] = React.useState(props.data.state);
  const [colorOpacity, setColorOpacity] = React.useState(props.data.brightness/100);
  const [anchor, setAnchor] = React.useState(null);
  const [style, setStyle] = React.useState(initialValue);
  const [regionName, setRegionName] = React.useState(props.name);
  const [level, setLevel] = React.useState(props.data.brightness);
  const [regionData, setRegionData] = React.useState(props.data);
  const [bg_color, setBgColor] = React.useState(rgbToHex(props.data.fx.params.color.r, props.data.fx.params.color.g, props.data.fx.params.color.b));
  const [iconColor, setIconColor] = React.useState(getIconColor(props.data.state));
 // var state = props.data.state;
  var color = props.data.fx.params.color;


  const [description, setDescription] = React.useState(
    "id: " +
      props.data.id +
      " range: <" +
      props.data.range.li +
      ":" +
      props.data.range.hi +
      "> " +
      props.data.fx.type
  );
  const [open, setOpen] = React.useState(true);
  const [goUp, setGoup] = React.useState();

  useImperativeHandle(ref, () => ({
    showRegion() {
      setOpen(true);
    },
    hideRegion(id_to_close) {
      setOpen(false);
    },
    goUpRegion(up, travelTo) {}
  }));

  const handleClickOpenEdit = () => {
    setOpenEdit(true);
  };

  const handleCloseEdit = () => {
    setOpenEdit(false);
  };

  const saveEditHandler = data => {
    

    setOpenEdit(false);
    setRegionName(data.name);
    setDescription(
      "id: " +
        data.id +
        " range: <" +
        data.range.li +
        ":" +
        data.range.hi +
        "> " +
        data.fx.type
    );
    // data.fx.params.color.r = color.r;
    // data.fx.params.color.g = color.g;
    // data.fx.params.color.b = color.b;
    data.brightness = level;
 //   data.state = data.state;
    
    
    setRegionData(data);

    RegionManager.updateRegion(data)
      .then(result => {
        props.updateRegion(data.id, data);
      })
      .catch(error =>props.alertDialog(error));

     // props.updateHandler();
  };

  const handleClickOpenFx = () => {
    setOpenFx(true);
  };

  const handleFromFxDialog = value => {
    setOpenFx(value);
  };

  const saveHandler = color_picker => {
    color = {r: color_picker.rgb.r, g: color_picker.rgb.g, b: color_picker.rgb.b };
    setOpenFx(false);

    setBgColor(color_picker.hex);

    let d = regionData;
    d.fx.params.color.r = color_picker.rgb.r;
    d.fx.params.color.g = color_picker.rgb.g;
    d.fx.params.color.b = color_picker.rgb.b;
    
    saveEditHandler(d);
  
  };

  const sliderValueChanged = (event, value) => {
    setColorOpacity(value / 100);
    setLevel(value);
  };

  const levelChanged  = (event, value) => {
    
    let d = regionData;
    d.brightness = value;

    saveEditHandler(d);
  };

  const handleAnchor = event => {
    setAnchor(event.currentTarget);
  };

  const resetAnchor = event => {
    setAnchor(null);
  };

  const handleEdit = event => {
    setAnchor(null);
    handleClickOpenEdit();
  };

  const handleRemove = event => {
    setAnchor(null);
    //  setOpen(false);
    props.removeCallback(props.id);
  };

  const handleOnOffClik = event => {
    let d = regionData;
    
    if (regionData.state === "on"){
      setIconColor(getIconColor("off"));
      d.state = "off";
    }
    else {
      setIconColor(getIconColor("on"));
      d.state = "on";
    }
  
    saveEditHandler(d);
    
  };

  function rgbToHex(r, g, b) {
    let c = "#" + ((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1);
    return c;
  }
  
  useEffect(() => { // 1

  });

  return (
    // <Grow in={open} style={style}>
    <Card className={classes.card}>
      <CardHeader
        action={
          <IconButton
            aria-label="settings"
            className={classes.more}
            onClick={handleAnchor}
          >
            <MoreVertIcon />
          </IconButton>
        }
        title={regionName}
        subheader={description}
        className={classes.header}
      />
      <RegionMenu
        anchor={anchor}
        closeMenu={resetAnchor}
        onEdit={handleEdit}
        onDelete={handleRemove}
      />
      <CardContent className={classes.content}>
        <Box mt={0}>
          {/* <Typography variant="body2" component="h2" >
              Brightness
          </Typography> */}

          <PrettoSlider defaultValue={level} onChange={sliderValueChanged} onChangeCommitted={levelChanged} />
        </Box>
      </CardContent>

      <Box mt={2} onClick={handleClickOpenFx}>
        <CardContent className={classes.content}>
          <Typography variant="body1" component="h3">
            <Box
              style={{ backgroundColor: bg_color, opacity: colorOpacity }}
              className={classes.fx}
              fontWeight="fontWeightBold"
            >
              Color
            </Box>
          </Typography>
        </CardContent>
      </Box>

      <ColorPicker
        open={openFx}
        color={bg_color}
        closeHandler={handleFromFxDialog}
        saveHandler={saveHandler}
      />

      <EditMenu
        open={openEdit}
        closeHandler={handleCloseEdit}
        saveHandler={saveEditHandler}
        title="Edit region"
        dataIn={props.data}
      />

      <CardActions className={classes.actions}>
        <Switch defaultChecked />

        <div className={classes.gap}></div>
        <Fab
          variant="round"
          color="primary"
          aria-label="add"
         // style={{ backgroundColor: "#394d00"}}
          className={classes.iconHover}
          onClick={handleOnOffClik}
        >
          <WbIncandescentIcon  style={{color: iconColor}} />
        </Fab>
      </CardActions>
    </Card>
    //  </Grow>
  );
});

const mapStateToProps = (state, ownProps) => {
  return {
    regions: state.regions
  };
};

const mapDispatchToProps = dispatch => {
  return {
    updateRegion: (index, region) =>
      dispatch(regionAction.updateRegion(index, region))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  { forwardRef: true }
)(Region);
