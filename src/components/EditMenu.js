import React from "react";
import { withStyles, makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import MuiDialogTitle from "@material-ui/core/DialogTitle";
import MuiDialogContent from "@material-ui/core/DialogContent";
import MuiDialogActions from "@material-ui/core/DialogActions";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import Typography from "@material-ui/core/Typography";
import { Box } from "@material-ui/core";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import OutlinedInput from "@material-ui/core/OutlinedInput";
import Input from "@material-ui/core/Input";
import Slider from "@material-ui/core/Slider";
import TextField from "@material-ui/core/TextField";
import MenuItem from "@material-ui/core/MenuItem";
import {RegionData, Range, Effect, Color} from '../data_structures';

import "./edit.css";

const styles = theme => ({
  root: {
    margin: 0,
    // padding: theme.spacing(2),
    paddingTop: 0,
    paddingBottom: 20
  },
  closeButton: {
    position: "absolute",
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500]
  },
  container: {
    display: "flex",
    flexWrap: "wrap"
  },
  formControl: {
    // margin: theme.spacing(1)
  }
});

const useStyles = makeStyles(theme => ({
  container: {
    display: "flex"
    //  flexWrap: "wrap"
  },
  formControl: {
    //margin: -10
    minWidth: 200
    //  marginLeft: theme.spacing(1),
    //   marginRight: theme.spacing(1),
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: 200
  },
  numberField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: 100 - theme.spacing(1)
  },
  root: {
    minWidth: 200,
    paddingBottom: 20,
    paddingTop: 0
  }
}));

function valuetext(value) {
  return `${value}°C`;
}

const DialogTitle = withStyles(styles)(props => {
  const { children, classes, onClose } = props;
  return (
    <MuiDialogTitle disableTypography style={{ padding: 16 }}>
      <Typography variant="h6">{children}</Typography>
      {onClose ? (
        <IconButton
          aria-label="close"
          className={classes.closeButton}
          onClick={onClose}
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

const DialogContent = withStyles(theme => ({
  root: {
    padding: theme.spacing(2)
  }
}))(MuiDialogContent);

const DialogActions = withStyles(theme => ({
  root: {
    margin: 0,
    padding: theme.spacing(1)
  }
}))(MuiDialogActions);

export default function CustomizedDialogs({
  open,
  closeHandler,
  saveHandler,
  title,
  dataIn
}) {
  const classes = useStyles();
  const labelRef = React.useRef(null);
  const [name, setName] = React.useState(dataIn.name);
  const [effect, setEffect] = React.useState(dataIn.fx.type);
  const [effectData, setEffectData] = React.useState([]);
  const [value, setValue] = React.useState([dataIn.range.li, dataIn.range.hi]);

  // if (dataIn) {
  //   setName(dataIn.name);
  //   setValue([dataIn.rangeL, dataIn.rangeH]);
  //   setEffect(dataIn.effect);
  // }

  const effects = [
    {
      value: "color",
      label: "color"
    },
    {
      value: "dimmer",
      label: "€"
    },
    {
      value: "gradient",
      label: "฿"
    },
    {
      value: "rainbow",
      label: "¥"
    },
    {
      value: "pingpong",
      label: "¥"
    }
  ];

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const handleClose = () => {
    closeHandler(false);
  };

  const save = () => {

    // to avoid mistakes, use region class
    let data = new RegionData(dataIn.id,
                          name, 
                          dataIn.state, 
                          10, 
                          new Range(value[0], value[1]), 
                          new Effect("color", dataIn.fx.params));
    saveHandler(data);
  };

  function handleNameChange(event) {
    setName(event.target.value);
  }

  function handleRangeLChange(event) {
    setValue([event.target.value, value[1]]);
  }

  function handleRangeHChange(event) {
    setValue([value[0], event.target.value]);
  }

  function handleEffectChange(event) {
    setEffect(event.target.value);
  }

  function handleChangeSlider(event, newValue) {
    setValue(newValue);
  }

  return (
    <div>
      <Dialog
        fullWidth={false}
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={open}
      >
        <DialogTitle
          id="customized-dialog-title"
          onClose={handleClose}
          className={classes.root}
        >
          {title}
        </DialogTitle>

        <DialogContent className={styles.root}>
          <Box
            style={{
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
              //minWidth: 300,
              spacing: 40
              //   margin: 100
              //      minHeight: 120
              //justifyContent: "center ",
            }}
          >
            <Box className={classes.root}>
              <TextField
                id="standard-name"
                label="Name"
                className={classes.textField}
                value={name}
                onChange={handleNameChange}
                margin="normal"
              />
            </Box>

            <Box
              style={{
                display: "flex",
                flexDirection: "row",
                alignItems: "center"
              }}
            >
              <Box style={{ minWidth: 50 }}>
                <TextField
                  id="standard-number"
                  label="Range start"
                  value={value[0]}
                  onChange={handleRangeLChange}
                  type="number"
                  className={classes.numberField}
                  InputLabelProps={{
                    shrink: true
                  }}
                  margin="normal"
                />
              </Box>

              <Box style={{ minWidth: 50 }}>
                <TextField
                  id="standard-number"
                  label="Range stop"
                  value={value[1]}
                  onChange={handleRangeHChange}
                  type="number"
                  className={classes.numberField}
                  InputLabelProps={{
                    shrink: true
                  }}
                  margin="normal"
                />
              </Box>
            </Box>

            <Box className={classes.root}>
              <Slider
                style={{ paddingTop: 50 }}
                id="slider-range"
                max={150}
                value={value}
                onChange={handleChangeSlider}
                valueLabelDisplay="auto"
                aria-labelledby="range-slider"
                getAriaValueText={valuetext}
              />
            </Box>

            <TextField
              id="standard-select-currency"
              select
              label="Effect"
              className={classes.textField}
              value={effect}
              onChange={handleEffectChange}
              SelectProps={{
                MenuProps: {
                  className: classes.menu
                }
              }}
              //  helperText="Please select your currency"
              margin="normal"
            >
              {effects.map(option => (
                <MenuItem key={option.value} value={option.value}>
                  {option.value}
                </MenuItem>
              ))}
            </TextField>
          </Box>
        </DialogContent>
        <DialogActions>
          <Button onClick={save} color="primary">
            Save changes
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
