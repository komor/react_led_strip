import React from "react";
import { Link, withRouter } from "react-router-dom";
import BottomNavigation from "@material-ui/core/BottomNavigation";
import BottomNavigationAction from "@material-ui/core/BottomNavigationAction";
import { makeStyles } from '@material-ui/core/styles';

import BarChartRoundedIcon from '@material-ui/icons/BarChartRounded';
import SettingsRoundedIcon from '@material-ui/icons/SettingsRounded';
import LineStyleRoundedIcon from '@material-ui/icons/LineStyleRounded';


const useStyles = makeStyles(theme => ({
    root: {
      flexGrow: 1,
    },
    views: {
      height : '90vh',
      padding: theme.spacing(2),
    },
    nav: {
     // backgroundColor : 'white',
     position: 'relative', 
     boxShadow: "0 30px 38px rgba(0,0,0,0.30), 0 15px 12px rgba(0,0,0,0.22)",
 //    zIndex : 0
    },
}));

function PrimaryNav () {

    const [ value, setValue ] =  React.useState(0);
    const classes = useStyles();

    return (
        <BottomNavigation
        value={value}
        onChange={(event, newValue) => {
            setValue(newValue);
        }}
        showLabels
        className={classes.nav}
        >
        <BottomNavigationAction
            label="Regions"
            icon={<LineStyleRoundedIcon />}
            component={Link}
            to="/"
        />
        <BottomNavigationAction
            label="FFT"
            icon={<BarChartRoundedIcon />}
            component={Link}
            to="fft"
        />
        <BottomNavigationAction
            label="Settings"
            icon={<SettingsRoundedIcon />}
            component={Link}
            to="settings"
        />
        </BottomNavigation>
    );
}

export default withRouter(PrimaryNav);
