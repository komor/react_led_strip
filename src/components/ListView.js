import Region from "../components/Region";
import { connect } from "react-redux";
import * as regionAction from "../actions/regionAction";

import React, {
  forwardRef,
  useImperativeHandle,
  useRef,
  createRef,
  useEffect
} from "react";
import { makeStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import { Box } from "@material-ui/core";
import { red } from "@material-ui/core/colors";
import ReactCSSTransitionGroup from "react-addons-css-transition-group";
import "./customScroll.css";
import FlipMove from "react-flip-move";
import CustomScroll from "react-custom-scroll";

import EditMenu from "./EditMenu";
import { RegionData, Range, Effect, Color } from "../data_structures";
import AlertDialog from "./AlertDialog";


const RegionManager = require("../services/RegionManager");



const useStyles = makeStyles(theme => ({
  fab2: {
    position: "relative",
    color: theme.palette.common.white,
    "&:hover": {
      backgroundColor: red[600]
    }
  },
  box: {
    display: "box",
    maxWidth: 500
  },
  extendedIcon: {
    marginRight: theme.spacing(1),
    padding: theme.spacing(2, 2)
  }
}));

const ListView = forwardRef((props, ref) => {
  const initialValue = [];
  const itemsRef = useRef([]);
  const [openEdit, setOpenEdit] = React.useState(false);
  const [firstRender, setFirstRender] = React.useState(true);
  const [alertText, setAlertText] = React.useState('');
  
  var alertRef = useRef();

  useImperativeHandle(ref, () => ({
    handleAdd() {
      setOpenEdit(true);
    }
  }));

  const alertDialog = text => {
    setAlertText(text);
    alertRef.current.openDialog();
  };

  const updateRegionList = () => {
    RegionManager.getRegions()
      .then(result => {

        props.setRegions(result.regions);
      })
      .catch(error => alertDialog(error));
  };

  const addRegion = data => {
    let id;
    RegionManager.addRegion(data)
      .then(result => {
        id = result.id;
        data.id = id;
        props.createRegion(data);
        updateRegionList();
      })
      .catch(error => alertDialog(error));
  };

  const removeRegion = id => {

    RegionManager.removeRegion(id)
    .then(result => {
      props.removeRegion(id);
    })
    .catch(error => alertDialog(error), updateRegionList());

  };

  const customEnterAnimation = {
    from: { transform: "scale(0.8, 0.8)" },
    to: { transform: "scale(1, 1)" }
  };

  const handleClickOpenEdit = () => {
    setOpenEdit(true);
  };

  const handleCloseEdit = () => {
    setOpenEdit(false);
  };

  const saveEditHandler = data => {
    setOpenEdit(false);
    addRegion(data);
  };

  useEffect(() => {
    if (firstRender) {
      updateRegionList();
      setFirstRender(false);
    }
  });
  

  return (
    <Box>
      <CustomScroll>
        <AlertDialog ref={alertRef} text={alertText}/>
        <EditMenu
          open={openEdit}
          closeHandler={handleCloseEdit}
          saveHandler={saveEditHandler}

          title="New region"
          dataIn={
            new RegionData(
              null,
              "Name",
              "off",
              10,
              new Range(0, 150),
              new Effect("color",  {color : new Color(255, 255, 255)})
            )
          }
        />

        <List
          style={{
            height: window.innerHeight - 56,
            width: 400,
            maxWidth: 500,
            position: "relative"
          }}
        >
          <FlipMove
            duration={300}
            easing="ease"
            enterAnimation={customEnterAnimation}
            leaveAnimation="fade"
          >
            {props.regions.map((item, i) => (
              <ListItem key={item.id}>
                <Region
                  name={item.name}
                  id={item.id}
                  removeCallback={removeRegion}
                  updateHandler={updateRegionList}
                  alertDialog={alertDialog}
                  data={item}
                  ref={el => (itemsRef.current[item.id] = el)}
                />
              </ListItem>
            ))}
          </FlipMove>
        </List>
      </CustomScroll>
    </Box>
  );
});

const mapStateToProps = (state, ownProps) => {
  return {
    regions: state.regions,
    id: state.id
  };
};

const mapDispatchToProps = dispatch => {
  return {
    createRegion: region => dispatch(regionAction.createRegion(region)),
    removeRegion: index => dispatch(regionAction.removeRegion(index)),
    addId: index => dispatch(regionAction.addId(index)),
    setRegions: (regions) => dispatch(regionAction.setRegions(regions))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  { forwardRef: true }
)(ListView);
