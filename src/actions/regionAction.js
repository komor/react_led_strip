
import * as actionTypes from './actionType';

export const createRegion = (region) => {
    return {
        type: actionTypes.CREATE_REGION,
        region: region
    }
};

export const removeRegion = (id) => {
    return {
        type: actionTypes.REMOVE_REGION,
        id: id
    }
};

export const updateRegion = (id, region) => {
    return {
        type: actionTypes.UPDATE_REGION,
        id: id,
        region: region
    }
};

export const setRegions = (regions) => {
    return {
        type: actionTypes.GET_ALL_REGIONS,
        regions: regions
    }
};

export const addId = (id) => {
    return {
        type: actionTypes.ADD_ID,
        id: id
    }
};
