export const CREATE_REGION = 'CREATE_REGION';
export const GET_ALL_REGIONS = 'GET_ALL_REGIONS';
export const REMOVE_REGION = 'REMOVE_REGION';
export const ADD_ID = 'ADD_ID';
export const UPDATE_REGION = 'UPDATE_REGION';
export const DARK_THEME = 'DARK_THEME';