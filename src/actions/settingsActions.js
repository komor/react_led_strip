import * as actionTypes from './actionType';

export const changeToDarkTheme = (dark) => {
    return {
        type: actionTypes.DARK_THEME,
        dark: dark
    }
};