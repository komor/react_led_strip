


class Color {
    constructor(r, g, b){
        this.r = r;
        this.g = g;
        this.b = b;
    }
}

class Effect {
    constructor(type, params){
        this.type = type;
        this.params = params;
    }
}


class Range {
    constructor(li, hi){
        this.li = li;
        this.hi = hi;
    }
}


class RegionData {
    constructor(id, name, state, brightness, range, fx){
       this.id = id;
       this.name = name;
       this.state = state;
       this.brightness = brightness;
       this.range = range;
       this.fx = fx; 
    }
}


// module.exports = {
//     'RegionData': RegionData,
//     'Range': Range,
//     'Effect': Effect,
//     'Color': Color

// };

export {RegionData, Range, Effect, Color};