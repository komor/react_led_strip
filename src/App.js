import React from "react";

import { BrowserRouter as Router, Route  } from "react-router-dom";
import PrimaryNav from "./components/PrimaryNav";
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';

// Views
import RegionsView from "./views/RegionsView";
import FFTView from "./views/FFTView";
import SettingsView from "./views/SettingsView";


import { withStyles } from '@material-ui/core/styles';



const useStyles = makeStyles(theme => ({
  root: {
   // display: 'flex',
    //flexDirection: 'column',

  },
  views: {
   height : '80vh',
   // padding: theme.spacing(2),
    flexGrow: 4,
  },
  nav: {
    //height : '15vh',
    margin: 0,
    top: 'auto',
    left: 0,
    bottom: 0,
    right: 'auto',
    position: 'fixed',
    width: '100vw',
    flexGrow: 1,
  },
}));

function App  () {
    const classes = useStyles();

    return (
        <Router >
          <div className="app">
            <Grid  
              className={classes.root}
              container
              direction="column"
              justify="center"
              alignItems="center"
            >
              <Grid item className={classes.views}>
                <Route exact path="/" component={RegionsView} />
                <Route path="/fft" component={FFTView} />
                <Route path="/settings" component={SettingsView} />
              </Grid>

              <Grid item className={classes.nav}>
                <PrimaryNav/>
              </Grid>
            </Grid> 
          </div>
        </Router>
    );
  }

export default withStyles({ withTheme: true }) (App);

