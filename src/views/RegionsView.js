import React, {useRef} from "react";
import { makeStyles } from "@material-ui/core/styles";
import ListView from "../components/ListView";
import { Box } from "@material-ui/core";

import AddIcon from "@material-ui/icons/Add";
import Fab from "@material-ui/core/Fab";

const useStyles = makeStyles(theme => ({
  fab: {
    margin: -28,
    // top: "auto",
    // right: 0,
    bottom: 100,
    left: "50%",
    //  marginRight: '-28px',
    //justifyContent: "center",
    position: "fixed",
    color: theme.palette.common.white,
    //padding: 0
    //  bottom: theme.spacing(2),
    // right: 60,
    backgroundColor: theme.palette.primary[700],
    "&:hover": {
      backgroundColor: theme.palette.common.orange
      // }
    }
  }
}));

export default function RegionsView() {
  const classes = useStyles();
  const listRef = useRef();

  return (
    <Box display="flex" justifyItems="center">
      <ListView ref={listRef} />
      <Box id="box" className={classes.box}>
        <Fab
          color="secondary"
          aria-label="add"
          className={classes.fab}
          onClick={() => listRef.current.handleAdd()}
        >
          <AddIcon />
        </Fab>
      </Box>
    </Box>
  );
}
