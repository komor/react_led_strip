import React from "react";
import { connect } from "react-redux";
import * as settingsActions from "../actions/settingsActions";
import Switch from "@material-ui/core/Switch";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import { theme } from "../components/Region";

function SettingsView(props) {
  //  props.changeToDarkTheme(false);
  return (
    <Box style={{
      padding: 16
      }}>
      <Box
        style={{
          display: "flex",
          flexDirection: "column",
          justifyContent: "flex-start",
          alignItems: "flex-start",
          width: 350,
          maxWidth: 500,
        }}
      >
        <Box
          style={{
            display: "flex",
            flexDirection: "column",
            justifyContent: "left",
            alignItems: "left",
            width: 350,
            maxWidth: 500,
            paddingBottom: 16
          }}
        >
          <Typography variant="h4" >
            Settings
          </Typography>
        </Box>
      </Box>

      <Box
        style={{ display: "flex", flexDirection: "row", alignItems: "center" }}
      >
        <Typography>Dark theme</Typography>
        <Box style={{ flexGrow: 2 }}></Box>
        <Switch
          color="primary"
          checked={props.dark}
          onChange={event => {
            props.changeToDarkTheme(event.target.checked);
          }}
        />
      </Box>
    </Box>
  );
}

const mapStateToProps = (state, ownProps) => {
  return {
    dark: state.dark
  };
};

const mapDispatchToProps = dispatch => {
  return {
    changeToDarkTheme: dark => dispatch(settingsActions.changeToDarkTheme(dark))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  { forwardRef: true }
)(SettingsView);
