import { combineReducers } from 'redux';
import regions from './regionReducer';
import id from './idReducer';
import dark from './settingsReducer';

export default combineReducers({
    regions: regions,
    id: id,
    dark: dark
});