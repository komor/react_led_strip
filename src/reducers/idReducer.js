import * as actionTypes from "../actions/actionType";

export default (state = 0 , action) => {
    switch (action.type) {
      case actionTypes.ADD_ID:
          return action.id;
      default:
        return state;
    }
};
  
