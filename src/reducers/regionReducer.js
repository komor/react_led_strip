import * as actionTypes from "../actions/actionType";


export default (state = [], action) => {
  switch (action.type) {
    case actionTypes.CREATE_REGION:
      return [...state, Object.assign({}, action.region)];
    case actionTypes.REMOVE_REGION:
      return state.filter((data, i) => data.id !== action.id);
    case actionTypes.UPDATE_REGION:
    {
      let temp =  state;
      let indexToUpdate = temp.findIndex(x => x.id == action.id);
      temp[indexToUpdate] = action.region;
      return temp;
    }  
        
    // return [
    //     ...state.filter((data, i) => data.id !== action.id),
    //     Object.assign({}, action.region)
    //   ];
    case actionTypes.GET_ALL_REGIONS:
      return action.regions;
    default:
      return state;
  }
};
