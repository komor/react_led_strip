import * as actionTypes from "../actions/actionType";

export default (state = true, action) => {
  switch (action.type) {
    case actionTypes.DARK_THEME:
      return action.dark;
    default:
      return state;
  }
};
