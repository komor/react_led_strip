
'use strict';

const axios = require('axios');

const baseApiUrl = 'http://192.168.1.48:5000/apiv2.0';
//import Region from '../data_structures';


const addRegion = (region) => {

    return new Promise((resolve, reject) => {
        axios
            .post(`${baseApiUrl}/region`, { 
                region
             })
            .then((result) => {
                resolve(result.data);
            })
            .catch(error => {
                console.log(error);
                reject(error.message);
            });

    });
};

const removeRegion = (id) => {

    return new Promise((resolve, reject) => {
        axios
            .delete(`${baseApiUrl}/region/`+id, { 
                
             })
            .then((result) => {
                resolve(result.data);
            })
            .catch(error => {
                console.log(error);
                
                reject(error.message);
            });

    });
};

const updateRegion = (region) => {

    return new Promise((resolve, reject) => {
        axios
            .put(`${baseApiUrl}/region/`+region.id, { 
                region
             })
            .then((result) => {
                resolve(result.data);
            })
            .catch(error => {
                console.log(error);
                reject(error.message);
            });

    });
};

const getRegions = () => {

    return new Promise((resolve, reject) => {
        axios
            .get(`${baseApiUrl}/regions`, { 
                
             })
            .then((result) => {
                resolve(result.data);
            })
            .catch(error => {
                console.log(error);
                reject(error.message);
            });

    });
};


module.exports = {
    'addRegion': addRegion,
    'removeRegion': removeRegion,
    'updateRegion': updateRegion,
    'getRegions': getRegions,
};