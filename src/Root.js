import React from "react";
import { createMuiTheme } from "@material-ui/core/styles";

import { MuiThemeProvider } from "@material-ui/core/styles";

import App from "./App";
import CssBaseline from "@material-ui/core/CssBaseline";
import {Helmet} from "react-helmet";
import { connect } from 'react-redux';
import * as settingsActions from './actions/settingsActions';
import './Root.css'

const font = "'Quicksand', sans-serif"; 

function Root(props) {

  var dark = {  
    palette: {
      type: "dark",
      primary: {
        light: "#99cc00",
        main: "#99cc00",
        dark: "#000"
      },
      secondary: {
        main: "#f44336"
      }
    },
    typography: {
      useNextVariants: true,
      fontFamily: 
        font,
    }
  };

  var light = {  
    palette: {
      type: "light",
      primary: {
        light: "#99cc00",
        main: "#99cc00",
        dark: "#000"
      },
      secondary: {
        main: "#f44336"
      }
    },
    typography: {
      useNextVariants: true,
      fontFamily: 
        font,
    }
  };

  const darkTheme = createMuiTheme(dark);
  const lightTheme = createMuiTheme(light);

  return (
    <MuiThemeProvider theme={props.dark ? darkTheme : lightTheme}>
      <CssBaseline />
      <Helmet>
         <meta
         name="viewport"
         content="width=device-width, initial-scale=1, user-scalable=0, maximum-scale=1, minimum-scale=1"
         />
      </Helmet>
   
      <App />
    </MuiThemeProvider>
  );
}

const mapStateToProps = (state, ownProps) => {
  return {
    dark: state.dark
  }
};

const mapDispatchToProps = (dispatch) => {
  return {
    changeToDarkTheme: (dark)  => dispatch(settingsActions.changeToDarkTheme(dark)),
  }
};

export default connect(mapStateToProps, mapDispatchToProps, null, {forwardRef: true})(Root);
